## device_lenovo_a536
Unofficial codename - alps

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | 1.3GHz Quad-Core MT6582M
GPU     | Mali-400MP
Memory  | 1GB RAM
Shipped Android Version | 4.4.2
Storage | 4GB
Battery | 2250 mAh
Display | 5" 854x480 px
Camera  | 8MPx

![Lenovo A536](https://static.svyaznoy.ru/upload/iblock/16d/2268905_0.jpg/resize/483x483/hq/)

This branch is for building LineageOS 14.1 ROMS.

#### Whats Working ?
 * Wifi
 * Bluetooth
 * Vibration
 * Audio
 * Flashlight (statusbar && Camera)
 * Video Recording
 * Micro SD support
 * MTP and Mass Storage modes
 * Rotation
 * All sensors
 * Doze
 * Offline charging
 * RIL (Call,Data,Msg)
 * Operator name
 * GPS
 * Audio over Bluetooth (music, calls .etc)
 * Hotspot
 
